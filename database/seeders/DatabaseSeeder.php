<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // вызаваем сидеры по списку
        $this->call([
            AuthorsSeeder::class,
            ArticlesSeeder::class,
            PostsSeeder::class,
            UsersSeeder::class,
            CitiesSeeder::class,
        ]);
    }
}
