<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use \Illuminate\Support\Str;

/**
 * Запустить можно так:
 * php artisan db:seed --class=AuthorsSeeder
 */

class AuthorsSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        DB::table('authors')->insert([
            [
                'username' => 'Andrey',
                'password' => Hash::make('1234'), // хешируем пароль
                'name' => 'Андрей',
                'age' => 37,
                'about' => 'Изучаю Лару',
                'dateOfBirth' => Date::createFromDate(1985, 2, 20, 'Europe/Moscow'),
                'registeredAt' => Date::now(),
                'lastLogin' => Date::now(),
                'avatar' => 'uplods/avatars/andrey' . Str::random(40) . '.png', // генерируем случайную строку на 256 символов
            ],
            [
                'username' => 'Pavel',
                'password' => Hash::make('4567'), // хешируем пароль
                'name' => 'Паша',
                'age' => 36,
                'about' => 'Кто-то',
                'dateOfBirth' => Date::createFromDate(1980, 11, 2, 'Europe/Moscow'),
                'registeredAt' => Date::now(),
                'lastLogin' => Date::now(),
                'avatar' => 'uplods/avatars/andrey' . Str::random(40) . '.png', // генерируем случайную строку на 256 символов
            ],
        ]);
    }
}
