<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            ['name' => 'Москва'],
            ['name' => 'Санкт-Петербург'],
            ['name' => 'Екатеринбург'],
            ['name' => 'Казань'],
            ['name' => 'Астрахань'],
            ['name' => 'Нижний Новгород'],
            ['name' => 'Ростов'],
            ['name' => 'Бобруйск'],
            ['name' => 'Елабуга'],
            ['name' => 'Ставрополь'],
        ]);
    }
}
