<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Str;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
                'title' => 'Пост блога 1',
                'slug' => 'post' . Str::random(10),
                'likes' => rand(0, 10000),
                'created_at' => Date::now(),
                'updated_at' => Date::now(),
            ],
            [
                'title' => 'Пост блога 2',
                'slug' => 'post' . Str::random(10),
                'likes' => rand(0, 10000),
                'created_at' => Date::now(),
                'updated_at' => Date::now(),
            ],
            [
                'title' => 'Пост блога 3',
                'slug' => 'post' . Str::random(10),
                'likes' => rand(0, 10000),
                'created_at' => Date::now(),
                'updated_at' => Date::now(),
            ],
        ]);
    }
}
