<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Кто-то 1',
                'email' => Str::random(6) . '@mail.ru',
                'age' => rand(14, 65),
                'salary' => rand(14000, 400000),
                'city_id' => rand(1, 10),
                'created_at' => Date::now(),
                'updated_at' => Date::now(),
            ],
            [
                'name' => 'Кто-то 2',
                'email' => Str::random(6) . '@mail.ru',
                'age' => rand(14, 65),
                'salary' => rand(14000, 400000),
                'city_id' => rand(1, 10),
                'created_at' => Date::now(),
                'updated_at' => Date::now(),
            ],
            [
                'name' => 'Кто-то 3',
                'email' => Str::random(6) . '@mail.ru',
                'age' => rand(14, 65),
                'salary' => rand(14000, 400000),
                'city_id' => rand(1, 10),
                'created_at' => Date::now(),
                'updated_at' => Date::now(),
            ],
        ]);
    }
}
