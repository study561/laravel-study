<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use \Illuminate\Support\Str;

/**
 * Запустить можно так:
 * php artisan db:seed --class=AuthorsSeeder
 */


class ArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert([
            [
                'title' => 'Первая статья',
                'text' => 'Бла-бла-бла, о чем-то...',
                'creationDate' => Date::now(),
                'titleImagePath' => 'uplods/posts/' . Str::random(40) . '.jpg',
            ],
            [
                'title' => 'Вторая статья',
                'text' => 'Еще одна болтология...',
                'creationDate' => Date::now(),
                'titleImagePath' => 'uplods/posts/' . Str::random(40) . '.jpg',
            ],
        ]);
    }
}
