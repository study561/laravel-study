<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * чтобы изменить структуру таблицы у фасада Schema надо вызвать статический метод table()
         */
        Schema::table('articles', function(Blueprint $table){
            $table->string('titleImagePath'); // добавим картнку к посту тут будет храниться ссылка на неё.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function(Blueprint $table){
            $table->dropColumn('titleImagePath');
        });
    }
};
