<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Миграция создана с помощью команды
 * php artisan make:migration create_users_table
 */

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id(); // задаем первичный ключ с именем колонки id
            // $table->increments('id'); // так можно задать первичный ключ со своим имененм
            $table->string('login'); // тип VARCHAR
            $table->string('password', 512); // тип VARCHAR с заданной длиной
            $table->string('name');
            $table->integer('age');
            $table->text('about'); // тип TEXT
            $table->date('dateOfBirth'); // тип DATE
            $table->dateTime('registeredAt'); // тип DATETIME
            $table->timestamp('lastLogin'); // тип TIMESTAMP
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Удаляем даблицу с проверкой на существование
        // Для удаления без проверки есть метод drop()
        Schema::dropIfExists('users');
    }
};
