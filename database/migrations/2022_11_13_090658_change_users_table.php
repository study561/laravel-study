<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * чтобы изменить структуру таблицы у фасада Schema надо вызвать статический метод table()
         */
        Schema::table('users', function(Blueprint $table){
            $table->string('avatar'); // ссылка на картинку с аватаром
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table){
            $table->dropColumn('avatar');
        });
    }
};
