<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    private const TABLE_NAME = 'users';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::TABLE_NAME, function(Blueprint $table){
            $table->string('password', 256)->change(); // нам на 512 пароль оказался не нужен
            $table->renameColumn('login', 'username'); // login нам не нравится, мы хотим username
            $table->text('about')->nullable()->change(); // сделаем поле "о себе" обнуляемым (при создании тоже можно ->nullable())
            // сделаем возраст по умолчанию null
            $table->integer('age')
                ->nullable() // обнуляемым
                ->default(null) // по умолчанию null
                ->unsigned() // беззнаковый
                ->change()
            ;
            // Добаим комментарий к полю "Дата родления"
            $table->date('dateOfBirth')
                ->comment('Дата рождения без точного времени, так как оно обычно неизвестно...')
                ->change()
            ;
            $table->string('name')->first()->change(); // имя на первое место
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function(Blueprint $table){
            $table->string('password', 512)->change(); // нет, все-таки нужен
            $table->renameColumn('username', 'login');
            $table->text('about')->nullable(false)->change(); // разобнуление
            $table->integer('age', false, true) // true во третьем параметре делает его обратно signed
                ->nullable(false) // делаем обратно необнуляемым
                ->change()
            ;
            // дефолтное значение убираем напрямую
            DB::statement('ALTER TABLE ' . self::TABLE_NAME . ' ALTER COLUMN age DROP DEFAULT');
            // TODO: не знаю как удалить коммент
            $table->string('name')->after('password')->change(); // возращаем поле name назад (после поля password)
        });
    }
};
