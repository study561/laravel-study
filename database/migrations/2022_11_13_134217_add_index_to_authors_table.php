<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('authors', function (Blueprint $table) {
            // сделаем логин уникальным
            $table->string('username')->unique()->change();
            // $table->unique('login'); // еще один способ
            // Так делается составной индекс
            // $table->index(['login', 'age']);
            // Можно задать свое название индекса
            // $table->unique('login', 'unique_login');
            // Можно переименовывать индексы
            // $table->renameIndex('from', 'to');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('authors', function (Blueprint $table) {
            // уберем уникальность логина (передаем массив)
            $table->dropUnique(['username']);
        });
    }
};
