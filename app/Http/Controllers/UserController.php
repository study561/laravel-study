<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\User;


class UserController extends Controller
{
    public function show()
    {
        return 'Hello, user!';
    }

    public function all()
    {
        return 'Show all users';
    }

    public function greet($surname, $name)
    {
        return "Hello, $name $surname";
    }

    // Для необязательного параметра в контроллере надо задать значение по умолчанию
    public function getCity($username = null)
    {
        $cityMappings = [
            'user1' => 'Moscow',
            'user2' => 'Kazan',
            'user3' => 'Novgorod',
            'user4' => 'Rostov',
        ];

        if (! $username || ! key_exists($username, $cityMappings)) {
            return 'Для такого юзера нет города';
        }

        return "Ваш город: {$cityMappings[$username]}";
    }

    public function greetFriend($n, $s)
    {
        // ключи массива становятся переменными в представлении
        return view('user.greetfriend', ['title' => 'Приветствие дорогого друга', 'name' => $n, 'surname' => $s]);
    }

    public function addEmployee($name, $age, $salary)
    {
        return view('user.addemployee', ['name' => $name, 'age' => $age, 'salary' => $salary]);
    }

    // Query builder

    public function getUsers()
    {
        
        echo 'QueryLog<br>';
        DB::enableQueryLog();
        $users = DB::table('users')->get();

        dump(DB::getQueryLog());
        DB::disableQueryLog();

        // collect($users)->map(function($user){
        //     dump($user);
        // });
        echo 'toSql<br>';
        $userSql = DB::table('users')->where('id', '=', 1)->toSql();
        dump($userSql);

        // Еще можно ->dd();
        echo '->dump()<br>';
        DB::table('users')->where('id', '=', 1)->dump();

        echo 'Select<br>';

        $notAllFields = DB::table('users')
            ->select('name', 'email')
            ->get()
        ;
        dump($notAllFields);

        $selectAlias = DB::table('users')
            ->select('name as Imya', 'age as Vozrast')
            ->get()
        ;
        dump($selectAlias);

        $withCondition = DB::table('users')
            ->where('age', '>', 50)
            ->get()
        ;

        dump($withCondition);

        $withCondition = DB::table('users')
            ->where('age', '>', 50)
            ->where('age', '<', 57) // AND
            ->get()
        ;

        dump($withCondition);

        $withCondition = DB::table('users')
            ->where('age', '>', 50)
            ->orWhere('id', '>', 1) // OR
            ->get()
        ;

        dump($withCondition);

        // сложный запрос

        $composite = DB::table('users')
            ->where('id', '>', 1)
            ->orWhere(function($query){
                $query
                    ->where('age', '>', 50)
                    ->where('age', '<', 57)
                ;
            })
            ->toSql()
        ;
        // select * from "users" where "id" > ? or ("age" > ? and "age" < ?)

        dump($composite);

        $first = DB::table('users')
            ->where('id', '>=', 1)
            ->first() // Получить только одну строку (первую, подходящую под условие)
        ;
        dump($first);
        // select * from "users" where "id" >= 1 limit 1

        // Получим почту у конкретного юзера
        $columnValue = DB::table('users')->where('id', '=', 2)->value('email');
        dump($columnValue);
        // select "email" from "users" where "id" = 2 limit 1

        // Получим коллекцию электронок всех юзеров
        $columnValuesCollection = DB::table('users')->pluck('email');
        dump($columnValuesCollection);
        // select "email" from "users"

        // Возраст в указанном диапазоне
        $between = DB::table('users')->whereBetween('age', [50, 100])->get();
        dump($between);
        // select * from "users" where "age" between 50 and 100

        // Возраст не в указанном диапазоне
        $notBetween = DB::table('users')->whereNotBetween('age', [50, 100])->get();
        dump($notBetween);
        // select * from "users" where "age" not between 50 and 100

        // Список конкретных значений
        $in = DB::table('users')->whereIn('id', [1,3])->get();
        dump($in);
        // select * from "users" where "id" in (1, 3)

        // Список всех значений, только не этих
        $notIn = DB::table('users')->whereNotIn('id', [1,3])->get();
        dump($notIn);
        // select * from "users" where "id" not in (1, 3)

        // Выберем юзеров с незаданной зп
        $whereNull = DB::table('users')->whereNull('salary')->get();
        dump($whereNull);
        // select * from "users" where "salary" is null

        // Выберем юзеров с заданной зп
        $whereNotNull = DB::table('users')->whereNotNull('salary')->get();
        dump($whereNotNull);
        // select * from "users" where "salary" is not null

        // Динамический where
        $whereDynamic = DB::table('users')->whereName('Кто-то 2')->get();
        dump($whereDynamic);
        // select * from "users" where "name" = 'Кто-то 2'

        $whereDynamic2 = DB::table('users')->whereCreated_at('2022-11-14 17:57:36')->get();
        dump($whereDynamic2);
        // select * from "users" where "created_at" = '2022-11-14 17:57:36'

        // Комбинированный динамический where
        $dynamicCommbined = DB::table('users')
            ->whereIdOrName(1, 'Кто-то 3') // id = 1 or name = 'Кто-то 3'
            ->get()
        ;
        dump($dynamicCommbined);
        // select * from "users" where "id" = 1 or "name" = 'Кто-то 3'

        $orderBy = DB::table('users')
            ->orderBy('salary', 'desc') // второй параметр по умолчанию asc
            ->get()
        ;
        dump($orderBy);
        // select * from "users" order by "salary" desc

        // Сортировка по дате
        $latest = DB::table('users')
            ->latest() // сортировка по убыванию поля created_at oldest() - по возрастанию
            ->get()
        ;
        dump($latest);
        // select * from "users" order by "created_at" desc

        $latest = DB::table('users')
            ->latest('updated_at') // можно указать конкретное поле для сортировки
            ->get()
        ;
        dump($latest);
        // select * from "users" order by "updated_at" desc

        // Случайная сортировка
        $randomOrder = DB::table('users')
            ->inRandomOrder()
            ->get()
        ;
        dump($randomOrder);
        // select * from "users" order by RANDOM()

        // Одна случайная запись
        $randomFirst = DB::table('users')
            ->inRandomOrder()
            ->first()
        ;
        dump($randomFirst);
        // select * from "users" order by RANDOM() limit 1

        // Количество получаемых записей
        $rowsNum = DB::table('users')
            ->take(2) // берем первые 2 записи
            ->get()
        ;
        dump($rowsNum);
        // select * from "users" limit 2

        // offset
        $skip = DB::table('users')
            ->skip(1) // пропускаем первую запись
            ->take(1) // при использовании skip() обязательно должен быть take()
            ->get()
        ;
        dump($skip);
        // select * from "users" limit 1 offset 1

        // Вставка данных
        $insert = DB::table('users')->insert([
            'name' => 'Кто-то ' . rand(3,50),
            'email' => Str::random(4) . '@mail.ru',
            'age' => rand(14, 80),
            'salary' => rand(40000, 150000),
            'city_id' => rand(1, 10),
            'created_at' => Date::now(),
            'updated_at' => Date::now(),
        ]);
        dump($insert); // true
        /*
        insert into "users" ("name", "email", "age", "salary", "created_at", "updated_at")
        values ('Кто-то 50', 'BEJ5@mail.ru', 64, 88580, '2022-11-18 15:20:02', '2022-11-18 15:20:02')
        */
        // Вставка данных и получение полученного id
        $insertGetId = DB::table('users')->insertGetId([
            'name' => 'Кто-то ' . rand(3,50),
            'email' => Str::random(4) . '@mail.ru',
            'age' => rand(14, 80),
            'salary' => rand(40000, 150000),
            'city_id' => rand(1, 10),
            'created_at' => Date::now(),
            'updated_at' => Date::now(),
        ]);
        dump($insertGetId); // 9
        /*
        insert into "users" ("name", "email", "age", "salary", "created_at", "updated_at")
        values ('Кто-то 38', 'NcwT@mail.ru', 79, 128412, '2022-11-18 15:23:54', '2022-11-18 15:23:54') returning "id"
        */

        // Массовая вставка
        // Вставка данных
        $inserMassive = DB::table('users')->insert([
            [
                'name' => 'Кто-то ' . rand(3,50),
                'email' => Str::random(4) . '@mail.ru',
                'age' => rand(14, 80),
                'salary' => rand(40000, 150000),
                'city_id' => rand(1, 10),
                'created_at' => Date::now(),
                'updated_at' => Date::now(),
            ],
            [
                'name' => 'Кто-то ' . rand(3,50),
                'email' => Str::random(4) . '@mail.ru',
                'age' => rand(14, 80),
                'salary' => rand(40000, 150000),
                'city_id' => rand(1, 10),
                'created_at' => Date::now(),
                'updated_at' => Date::now(),
            ]
        ]);
        dump($inserMassive); // true
        /*
        insert into "users" ("age", "created_at", "email", "name", "salary", "updated_at")
        values (52, '2022-11-18 15:26:13', 'n9HH@mail.ru', 'Кто-то 8', 104469, '2022-11-18 15:26:13'),
                (66, '2022-11-18 15:26:13', '9egi@mail.ru', 'Кто-то 49', 49288, '2022-11-18 15:26:13')
        */

        // Обновление записей
        DB::table('users')->where('id' , 1)->update([ // обновим одну запись
                'salary' => 45000,
        ]);
        // update "users" set "salary" = 45000 where "id" = 1

        DB::table('users')->whereBetween('id', [2, 4])->update([ // обновим группу записей
            'salary' => 75000,
        ]);
        // update "users" set "salary" = 75000 where "id" between 2 and 4

        /*
        DB::table('users')->update([ // обновим все записи
            'salary' => 100000,
        ]);
        */
        // update "users" set "salary" = 100000

        // Инкремент / декремент
        DB::table('users')
            ->where('id', 2)
            ->increment('age') // по умолчанию увеличиваем на 1
        ;
        // update "users" set "age" = "age" + 1 where "id" = 2

        DB::table('users')
            ->where('id', 3)
            ->decrement('salary', 100) // вторым параметром указываем насколько уменьшить
        ;
        // update "users" set "salary" = "salary" - 100 where "id" = 3

        // Удаление записей
        DB::table('users')
            ->where('id', 10)
            ->delete()
        ;
        // delete from "users" where "id" = 10

        // Удалить всех юзеров
        // DB::table('users')->delete();
        // delete from "users"

        return view('user.getall', ['users' => $users]);
    }

    public function joinExample()
    {
        // Связывание таблиц
        $leftJoin = DB::table('users')
            ->select('users.*', 'cities.name as city')
            ->leftJoin('cities', 'cities.id', '=', 'users.city_id')
            ->get()
        ;
        dump($leftJoin);
    }


    // Eloquent

    public function getAll()
    {
        $withCondition = User::where('id', '=', 1)->get();
        dump($withCondition);

        $findOne = User::find(2); // получаем юзера с id=2
        dump($findOne);

        $findMany = User::find([2, 3]); // получаем юзеров с id 2 и 3
        dump($findMany);
        // select * from "users" where "users"."id" in (2, 3)

        $users = User::all();
        return view('user.getall', ['users' => $users]);
    }

    public function addUser()
    {
        $names = ['Дима', 'Вася', 'Коля', 'Гена', 'Степа'];

        // Создание новых юзеров в БД
        $userModel = new User;

        $userModel->name = $names[array_rand($names)]; // случайное имя
        $userModel->email = Str::random(5) . '@mail.ru';
        $userModel->age = rand(14, 90);
        $userModel->salary = rand(20000, 200000);
        $userModel->city_id = rand(1, 10);

        dump(
            $userModel->save()
        );
        // поля created_at и updated_at будут установлены автоматически. 
    }

    public function changeFirstUser()
    {
        $user = User::find(1);
        $user->salary = 150000;
        $user->save();

        dump($user);
    }

    public function deleteById($id)
    {
        // Другие способы
        // User::where('id', '=', 1)->delete;
        // User::find($id)->delete();

        User::destroy($id);

        // Можно и так
        // User::destroy([1, 2, 3]);
    }

    public function getSoftDeleted($id)
    {
        // Удалим юзера
        User::destroy($id);

        // а теперь получим всех узеров, включая удаленного
        $allUsers = User::withTrashed()->get();
        dump($allUsers);
    }

    public function restoreById($id)
    {
        // восстановим юзера
        User::withTrashed()->where('id', $id)->restore();
        $user = User::find($id);

        dump($user);
    }
}
