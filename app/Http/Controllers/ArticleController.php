<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function render()
    {
        $days = [];
        // 't' - количество дней в текущем месяце
        foreach(range(1, (new \DateTime)->format('t'), 1) as $day) {
            $days[] = $day;
        }

        return view('article.render', [
            'class' => 'bold',
            'phone' => '79091590500',
            'email' => 'vorojan@mail.ru',
            'age' => 37,
            'styleColor' => 'red',
            'linkText' => 'Сайт Ворожко Андрея',
            'linkHref' => 'https://vorozhko.ru',
            'employee' => ['name' => 'Андрей', 'age' => 37, 'salary' => 200000],
            'children' => ['Миша', 'Женя'],
            'city' => false,
            'location' => ['city' => null, 'country' => false],
            'lastSeen' => ['year' => null, 'month' => null, 'day' => null],
            'warning' => '<b>Этот текст будет жирным, так как не применится функция htmlentities() для тега {!! $warning !!}</b>',
            'crimes' => [],
            // Практика
            'links' => [
                [
                    'text' => 'text1',
                    'href' => 'href1',
                ],
                [
                    'text' => 'text2',
                    'href' => 'href2',
                ],
                [
                    'text' => 'text3',
                    'href' => 'href3',
                ],
            ],
            'employees' => [
                [
                    'name' => 'user1',
                    'surname' => 'surname1',
                    'salary' => 1000,
                ],
                [
                    'name' => 'user2',
                    'surname' => 'surname2',
                    'salary' => 2000,
                ],
                [
                    'name' => 'user3',
                    'surname' => 'surname3',
                    'salary' => 3000,
                ],
                [
                    'name' => 'user4',
                    'surname' => 'surname4',
                    'salary' => 4000,
                ],
                [
                    'name' => 'user5',
                    'surname' => 'surname5',
                    'salary' => 5000,
                ],
            ],
            'users' => [
                [
                    'name' => 'user1',
                    'surname' => 'surname1',
                    'banned' => true,
                ],
                [
                    'name' => 'user2',
                    'surname' => 'surname2',
                    'banned' => false,
                ],
                [
                    'name' => 'user3',
                    'surname' => 'surname3',
                    'banned' => true,
                ],
                [
                    'name' => 'user4',
                    'surname' => 'surname4',
                    'banned' => false,
                ],
                [
                    'name' => 'user5',
                    'surname' => 'surname5',
                    'banned' => false,
                ],
            ],
            'inputs' => [
                'магазин',
                'салон',
                'гипермаркет',
                'шоурум',
            ],
            'options' => [
                'шапка',
                'шляпа',
                'панама',
                'папаха',
            ],
            'days' => $days,
            'currentDay' => (new \DateTime)->format('j'),
        ]);
    }
}
