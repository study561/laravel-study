<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class TrainController extends Controller
{
    public function trainCollections()
    {
        $digits = range(1, 4);
        $daysCollection = collect($digits);

        $digitsAndNulls = [null, ...$digits, null];

        // fluent interface - можно использовать
        // т.к. всегда возвращается новый объект коллекции
        $dplus = collect($digitsAndNulls)
            ->map(function($item){
                // ко всем непустым добавим единицу
                return $item ? ++$item : null;
            })
            ->reject(function($item){
                // отфильтруем налы
                return empty($item);
            })
        ;

        // Коллекции макропрограммируемы
        Collection::macro('nullToZero', function() {
            return $this->map(function($item) {
                return $item ?: 0;
            });
        });

        // Аргументы макропрограммы
        Collection::macro('mutiplyTo', function($to) {
            return $this->map(function($item) use ($to) {
                return $item * $to;
            });
        });

        // TODO: коллекции надо поучить поглубже, венусь позже

        return view('train.collections', [
            'basic' =>
                [
                    'return' =>
                    [
                        'result' => $dplus->all(),
                        'comment' => 'Коллекции неизменяемы, возвращается новый экземпляр'
                    ],
                    'macro' => 
                    [
                        'result' => collect($digitsAndNulls)->nullToZero()->all(),
                        'comment' => 'Коллекции макропрограммируемы',
                    ],
                    'macroArgs' =>
                    [
                        'result' => $dplus->mutiplyTo(5)->all(),
                        'comment' => 'Макропрограмма с аргументом',
                    ],
                ],
            'methods' =>
            [
                'all' =>
                [
                    'result' => $daysCollection->all(),
                    'comment' => 'Метод all() возвращает исходный массив',
                ],
                'avg' =>
                [
                    'result' => collect([['count' => 2], ['count' => 4], ['noconut' => 100500]])->avg('count'),
                    'comment' => 'Метод avg() считает среднее значение переданного ключа',
                ],
                'chunk' =>
                [
                    'result' => collect(range(1,10))->chunk(3)->all(),
                    'comment' => 'Метод chunk() разбивает коллекцию на несколько меньших коллекций указанного размера',
                ],
                'chunkWhile' =>
                [
                    'result' => collect(str_split('AAAVVZ'))->chunkWhile(function ($value, $key, $chunk) {
                        // довавляем элемент в текущую подколлекцию если он тождественен предыдущему
                        return $value === $chunk->last();
                    })->all(),
                    'comment' => 'Метод chunkWhile() разбивает коллекцию c посощью переданног колбека',
                ],
            ],
        ]);
    }
}
