<x-layout>
	<x-slot:title>
		{{ $title }} :)
	</x-slot>
	Привет, дорогой друг, {{ $name }}, {{ $surname }}!
</x-layout>