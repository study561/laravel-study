<x-layout>
	<x-slot:title>
		Все юзеры
	</x-slot>
    <table>
        <th>id</th>
        <th>name</th>
        <th>email</th>
        <th>age</th>
        <th>created_at</th>
        <th>updated_at</th>
    @foreach ($users as $user)
    <tr>
        <td>{{ $user->id }}</td>
        <td>{{$user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->age }}</td>
        <td>{{ $user->created_at }}</td>
        <td>{{ $user->updated_at }}</td>
    </tr>
    @endforeach
    </table>
</x-layout>
