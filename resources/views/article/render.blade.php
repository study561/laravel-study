<x-layout>
	<style>
		.bold {
			font-weight: bold;
		}
		.active {
			color: red;
			font-weight: bold;
		}
	</style>
	<x-slot:title>
		Это статья на сайте
	</x-slot>
		<p class="{{ $class }}">Это жирный абзац</p>
		<form action="">
			<input type="text" name="phone" value="{{ $phone }}">
			<input type="text" name="email" value="{{ $email }}">
			<input type="text" name="age" value="{{ $age }}">
		</form>
		<p style="color:{{ $styleColor }};">Этот абзац красный</p>
		<p>Ссылка на <a href="{{ $linkHref }}">{{ $linkText }}</a></p>

		<p>Дата в формате день.месяц.год: {{ (new \DateTime)->format('d.F.Y') }}</p>
		<h1>Сведения о работнике</h1>
		<p>Имя: {{ $employee['name'] }}</p>
		<p>Возраст: {{ $employee['age'] }}</p>
		<p>Заработок: {{ $employee['salary'] }} руб.</p>
		<p>Количество детей: {{ count($children) }}</p>
		<p>Город по прописке: {{ $city ?: 'Москва' }}</p>
		<p>Текущее место обитания: {{ $location['country'] ?: 'Россия' }}, {{ $location['city'] ?? 'Москва' }}</p>
		<p>
			Последний раз его видели: 
			{{ $lastSeen['day'] ?? (new \DateTime)->format('d') }}-
			{{ $lastSeen['month'] ?? (new \DateTime)->format('F') }}-
			{{ $lastSeen['year'] ?? (new \DateTime)->format('Y') }}
		</p>
		<p>Внимание: {!! $warning !!}</p>
		<p>Это комментарий в блейд-шаблоне:  {{-- Этот комментарий не будет выведен в HTML коде страницы --}} что там написано вы никогда не узнаете :)</p>


		<p>Этот контент только для взрослых:</p>
		@if ($age > 18)
		<p>Ԑ::::э</p>
		@elseif ($age == 18)
		<p>.l.</p>
		@else
		<p>Такое деткам не показывают</p>
		@endif

		<p>Этот текст только для несовершеннолетних:</p>
		{{-- то есть текст не покажется пока возраст больше 18 то же что @if (! ($age > 18)) --}}
		@unless ($age > 18)
		<p>У ти какой пупсик )))</p>
		@endunless

		<p>Есть ли у вас дети?</p>
		@if (count($children) > 0)
			<p>Да, вы родитель</p>
		@else
			<p>Нет у вас детей</p>
		@endif

		<p>Как зовут ваших детей?</p>
		<ol>
		@foreach ($children as $kid)
			<li>{{ $kid }}</li>
		@endforeach
		</ol>


		<p>Еще раз о работнике:</p>
		<ul>
		@foreach ($employee as $key => $value)
			<li>{{ $key }}: {{ $value }}</li>
		@endforeach
		</ul>

		<p>Участвовал ли в темных делишках:</p>
		@forelse ($crimes as $crime)
		<p>{{ $crime }}</p>
		@empty
		<p>не участвовал :)</p>
		@endforelse

		<p>Переменная $loop</p>
		<p>Первый и последний</p>
		@foreach ($employee as $key => $value)
			@if ($loop->first)
			<p><b>{{ $key }}</b>: {{ $value }}</p>
			@elseif ($loop->last)
			<p><i>{{ $key }}</i>: {{ $value }}</p>
			@else
			<p>{{ $key }}: {{ $value }}</p>
			@endif
		@endforeach

		<p>Четность и нечетность</p>
		@foreach ($employee as $key => $value)
			{{-- нечетность --}}
			@if ($loop->odd)
			<p><b>{{ $key }}</b>: {{ $value }}</p>
			{{-- четность --}}
			@elseif ($loop->even)
			<p><i>{{ $key }}</i>: {{ $value }}</p>
			@endif
		@endforeach

		<p>Текущая итерация начиная с нуля</p>
		@foreach ($employee as $key => $value)
		<p>[{{ $loop->index }}] - {{ $key }}: {{ $value }}</p>
		@endforeach

		<p>Текущая итерация начиная с единицы</p>
		@foreach ($employee as $key => $value)
		<p>[{{ $loop->iteration }}] - {{ $key }}: {{ $value }}</p>
		@endforeach

		<p>Количество элементов массива</p>
		@foreach ($employee as $key => $value)
		<p>[{{ $loop->count }}] - {{ $key }}: {{ $value }}</p>
		@endforeach

		<p>Разрыв цикла (на втором элемете выходим)</p>
		@foreach ($employee as $key => $value)
			@if ($loop->index == 1)
				@break;
			@endif
			<p>{{ $key }}: {{ $value }}</p>
		@endforeach

		<p>Пропуск итерации (пропускаем первый элемент)</p>
		@foreach ($employee as $key => $value)
			@if ($loop->iteration == 1)
				@continue;
			@endif
			<p>{{ $key }}: {{ $value }}</p>
		@endforeach

		<p>Цикл for</p>
		@for ($i = 1; $i <= 10; $i++)
			<p>{{ $i }}<p>
		@endfor

		<p>Блок чистого php<p>
		@php
		print_r($employee);
		@endphp

		<h2>Практика</h2>
		<p>Задание 1</p>
		<p>
		@foreach ($links as $link)
			<a href="http://{{ $link['href'] }}">{{ $link['text'] }}</a>
		@endforeach
		</p>
		<p>Задание 2</p>
		<ul>
		@foreach ($links as $link)
			<li><a href="http://{{ $link['href'] }}">{{ $link['text'] }}</a></li>
		@endforeach
		</ul>
		<p>Задание 3</p>
		<table>
		@foreach ($employees as $e)
			<tr>
				<td>{{ $e['name'] }}</td>
				<td>{{ $e['surname'] }}</td>
				<td>{{ $e['salary'] }}</td>
			</tr>
		@endforeach
		</table>
		<p>Задание 4</p>
		<table>
			<tr>
				<th>Имя</th>
				<th>Фамилия</th>
				<th>Зарплата</th>
			</tr>
		@foreach ($employees as $e)
			<tr>
				<td>{{ $e['name'] }}</td>
				<td>{{ $e['surname'] }}</td>
				<td>{{ $e['salary'] }}</td>
			</tr>
		@endforeach
		</table>
		<p>Задание 5</p>
		<table>
			<tr>
				<th>Номер</th>
				<th>Имя</th>
				<th>Фамилия</th>
				<th>Зарплата</th>
			</tr>
		@foreach ($employees as $e)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $e['name'] }}</td>
				<td>{{ $e['surname'] }}</td>
				<td>{{ $e['salary'] }}</td>
			</tr>
		@endforeach
		</table>
		<p>Задание 6</p>
		<table>
			<tr>
				<th>Номер</th>
				<th>Имя</th>
				<th>Фамилия</th>
				<th>Зарплата</th>
			</tr>
		@foreach ($employees as $e)
			@if ($e['salary'] < 2000)
				@continue;
			@endif
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{ $e['name'] }}</td>
				<td>{{ $e['surname'] }}</td>
				<td>{{ $e['salary'] }}</td>
			</tr>
		@endforeach
		</table>
		<p>Задание 7</p>
		<table>
			<tr>
				<th>Имя</th>
				<th>Фамилия</th>
				<th>Статус</th>
			</tr>
		@foreach ($users as $u)
			<tr>
				<td>{{ $u['name'] }}</td>
				<td>{{ $u['surname'] }}</td>
				<td>{{ $u['banned'] ? 'забанен' : 'активен' }}</td>
			</tr>
		@endforeach
		</table>
		<p>Задание 8</p>
		<table>
			<tr>
				<th>Имя</th>
				<th>Фамилия</th>
				<th>Статус</th>
			</tr>
		@foreach ($users as $u)
			<tr>
				<td>{{ $u['name'] }}</td>
				<td>{{ $u['surname'] }}</td>
				<td style="color:{{ $u['banned'] ? 'red' : 'green' }};">{{ $u['banned'] ? 'забанен' : 'активен' }}</td>
			</tr>
		@endforeach
		</table>
		<p>Задание 8</p>
		<p>
		@foreach ($inputs as $input)
			<input type="text" value="{{ $input }}">
		@endforeach
		</p>
		<p>Задание 9</p>
		<p>
		<select>
		@foreach ($options as $option)
			<option value="{{ $option }}">{{ $option }}</option>
		@endforeach
		</select>
		</p>
		<p>Задание 10</p>
		<ul>
		@foreach ($days as $day)
			<li{{ $day == $currentDay ? ' class=active ' : '' }}>{{ $day }}</li>
		@endforeach
		</ul>
</x-layout>