<x-layout>
	<x-slot:title>
		Прокачка коллекций
	</x-slot>
    <h1>Базовые возможности</h1>
    @foreach ($basic as $rule)
        <p>
        @php
            print_r($rule['result']);
        @endphp
        <br>
        {{ $rule['comment'] }}
        </p>
    @endforeach
    <h1>Методы</h1>
    @foreach ($methods as $method)
        <p>
        @php
            print_r($method['result']);
        @endphp
        <br>
        {{ $method['comment'] }}
        </p>
    @endforeach
</x-layout>