<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\TrainController;
use App\Http\Controllers\UserController;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Свой маршрут
Route::get('/study', function() {
    dump(env('APP_DEBUG'));
    dump(App::environment());
    dump(config('app.timezone', 'Europe/Moscow'));
    dd(config('database.connections.pgsql.database'));
    return 'Let the study begins!';
});

// Можно задать вообще любой маршрут
Route::get('/study/begins', function() {
    return 'Let the study begins!';
});

// Параметры маршрута - важен порядок следования а не имена переменных
// surname прилетает первым параметром в метод greet(), 
Route::get('/user/{surname}/{name}', [UserController::class, 'greet']);

// Необязательные параметры маршрута со значением по умолчанию
Route::get('/city/{city?}', function($city = 'Moscow') {
    return "Your city is $city";
});

// Ограничения на параметры маршрута
Route::get('/category/{id}', function($id) {
    return "Category with numeric id: $id";
})->where('id', '\d+');

// Ограничение нескольких параметров
Route::get('/product/{id}/{brand}', function($id, $brand) {
    return "Product numeric id: $id and string brand: $brand";
})->where('id', '\d+')->where('brand', '\w{2,8}');

// Шаблонные ограничения (name - только буквы)
Route::get('/task/{name}', function($name) {
    return "Your task is $name";
})->whereAlpha('name');

// Глобальные ограничения: параметр article в любых маршрутах
// должен начинаться на vz а дальше любое количество цифр 
Route::get('/good/{article}', function($article) {
    return "Good's article is $article";
});

// Группировка маршрутов
/*
Route::get('/admin/users', function ($id) {
    return 'all';
});
Route::get('/admin/user/{id}', function ($id) {
    return $id;
});
*/
Route::prefix('admin')->group(function() {
    Route::get('/users', function () {
        return 'all';
    });
    Route::get('/user/{id}', function ($id) {
        return $id;
    });
});

// Именованные маршруты
// имя потом можно использовать для различных целей
Route::get('/user/profile', function () {
    return 'profile';
})->name('profile');

// Изучаем контроллеры

// Вызываем контроллер UserController и метод show() в нем
Route::get('/show/user', [UserController::class, 'show']);

Route::get('/show/user/all', [UserController::class, 'all']);

// $id полетит первым параметром в метод 'show'
Route::get('/post/{id}', [PostController::class, 'show']);

// Для необязательного параметра в контроллере надо задать значение по умолчанию
Route::get('/city/by/user/{username?}', [UserController::class, 'getCity']);

// Подкючаем представление в контроллере
Route::get('/article/view', [ArticleController::class, 'render']);

Route::get('/greet/friend/{name}-{surname}', [UserController::class, 'greetFriend']);

Route::get('/user/emloyee/add/{name}/{age}/{salary}', [UserController::class, 'addEmployee']);

Route::get('/train/collections', [TrainController::class, 'trainCollections']);

Route::get('/users/getall',[UserController::class, 'getUsers']);

Route::get('/users/join/example',[UserController::class, 'joinExample']);

// Получение всех юзеров через модель
Route::get('/users/all', [UserController::class, 'getAll']);

// Добавление юзера через модель
Route::get('/users/addnew', [UserController::class, 'addUser']);

// Изменение юзера в модели
Route::get('/users/change/first', [UserController::class, 'changeFirstUser']);

// Удаление пользователя
Route::get('/users/delete/by/id/{id}', [UserController::class, 'deleteById']);

// Для тестирования softdeletable
Route::get('/users/soft/delete/{id}', [UserController::class, 'getSoftDeleted']);

// Для тестирования softdeletable восстановления
Route::get('/users/soft/restore/{id}', [UserController::class, 'restoreById']);
